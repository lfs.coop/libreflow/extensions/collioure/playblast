from kabaret import flow
from libreflow.utils.flow.extension_manager import classqualname


# Playblast settings
# ---------------------------------


class RenderCameraItem(flow.Object):

    camera_name = flow.Param()
    resolution_width = flow.IntParam(0)
    resolution_height = flow.IntParam(0)
    output_names = flow.Param(dict)


class AddRenderCameraItem(flow.Action):

    camera_name = flow.SessionParam('')
    resolution_width = flow.SessionParam(0)
    resolution_height = flow.SessionParam(0)
    output_names = flow.SessionParam(dict)

    _map = flow.Parent()

    def get_buttons(self):
        return ['Add', 'Cancel']

    def run(self, button):
        if button == 'Cancel':
            return
        
        ci = self._map.add(f'c{len(self._map):04}')
        ci.camera_name.set(self.camera_name.get())
        ci.resolution_width.set(self.resolution_width.get())
        ci.resolution_height.set(self.resolution_height.get())
        ci.output_names.set(self.output_names.get())
        self._map.touch()


class RenderCameraMap(flow.Map):

    add_camera = flow.Child(AddRenderCameraItem)

    @classmethod
    def mapped_type(cls):
        return RenderCameraItem
    
    def columns(self):
        return ('Name', 'Resolution', 'Output names')
    
    def _fill_row_cells(self, row, item):
        row['Name'] = item.camera_name.get()
        row['Resolution'] = f'{item.resolution_width.get()}x{item.resolution_height.get()}'
        row['Output names'] = item.output_names.get()


class PlayblastSettings(flow.Object):

    render_cameras = flow.Child(RenderCameraMap)


# Main action
# ---------------------------------


class RevisionNameChoiceValue(flow.values.SessionValue):

    DEFAULT_EDITOR = 'choice'
    STRICT_CHOICES = False

    _file = flow.Parent(2)

    def __init__(self, parent, name):
        super(RevisionNameChoiceValue, self).__init__(parent, name)
        self._revision_names = None

    def choices(self):
        if self._revision_names is None:
            self._revision_names = self._file.get_revision_names(sync_status='Available', published_only=True)
        
        return self._revision_names
    
    def revert_to_default(self):
        names = self.choices()
        if names:
            self.set(names[-1])
    
    def touch(self):
        self._revision_names = None
        super(RevisionNameChoiceValue, self).touch()


class ResolutionChoiceValue(flow.values.SessionValue):

    DEFAULT_EDITOR = 'choice'

    def choices(self):
        return ['25', '50', '100']


class CameraSelection(flow.values.SessionValue):

    DEFAULT_EDITOR = 'multichoice'

    _task = flow.Parent(4)

    def choices(self):
        settings = self.root().project().admin.project_settings._mng.get_related('playblast_settings')
        return [
            c.camera_name.get()
            for c in settings.render_cameras.mapped_items()
            if self._task.name() in c.output_names.get()
        ]


class RenderPlayblast(flow.Action):

    ICON = ('icons.libreflow', 'blender')

    revision = flow.SessionParam(None, RevisionNameChoiceValue)
    resolution_percentage = flow.SessionParam('100', ResolutionChoiceValue).ui(
        label='Resolution scale (%)'
    )
    cameras = flow.SessionParam([], CameraSelection)

    with flow.group('Advanced'):
        reduce_textures = flow.SessionParam(False).ui(
            tooltip='Reduce texture sizes before render, to reduce memory footprint',
            editor='bool',
        )
        target_texture_width = flow.SessionParam(4096).ui(
            tooltip='Size to reduce textures to',
            editor='int',
        )

    _file = flow.Parent()
    _task = flow.Parent(3)

    def allow_context(self, context):
        settings = self.root().project().admin.project_settings._mng.get_related('playblast_settings')
        return (
            context
            and self._file.format.get() == 'blend'
            and self.revision.choices()
            and bool(settings.render_cameras.mapped_names())
        )
    
    def needs_dialog(self):
        if self.cameras.choices():
            self.message.set('<h2>Render playblast</h2>')
            self.revision.touch()
            self.revision.revert_to_default()
        else:
            self.message.set(
                '<h2>Render playblast</h2>'
                '<font color=#D66700><b>No camera set up for this task. '
                'See project settings > playblast to configure at least one.</b></font>')
        return True
    
    def get_buttons(self):
        if not self.cameras.choices():
            return ['Cancel']
        else:
            return ['Render', 'Cancel']
    
    def run(self, button):
        if button == 'Cancel':
            return
        
        settings = self.root().project().admin.project_settings._mng.get_related('playblast_settings')
        camera_names = {
            c.camera_name.get(): c.name()
            for c in settings.render_cameras.mapped_items()
        }
        
        for name in self.cameras.get():
            c = settings.render_cameras[camera_names[name]]
            self._file.render_blender_playblast.revision_name.set(self.revision.get())
            self._file.render_blender_playblast.resolution_percentage.set(self.resolution_percentage.get())
            self._file.render_blender_playblast.reduce_textures.set(self.reduce_textures.get())
            self._file.render_blender_playblast.target_texture_width.set(self.target_texture_width.get())
            self._file.render_blender_playblast.render_camera.set(c.camera_name.get())
            self._file.render_blender_playblast.resolution_width.set(c.resolution_width.get())
            self._file.render_blender_playblast.resolution_height.set(c.resolution_height.get())
            self._file.render_blender_playblast.playblast_name.set(c.output_names.get()[self._task.name()])
            self._file.render_blender_playblast.run('Render')


def get_handlers():
    return [
        ("^/[^/]+/admin/project_settings$", "playblast_settings", classqualname(PlayblastSettings), {'label': 'Playblast'}),
        ("^/[^/]+/films/[^/]+/sequences/[^/]+/shots/[^/]+/tasks/[^/]+/files/[^/]+_blend$", "render_playblast_clr", classqualname(RenderPlayblast), {'label': 'Render playblast Collioure'})
    ]